#!/usr/bin/env python3

import sys, getopt, datetime
from xml.dom import minidom

# Global variables
textList = []
inputfile = ''
outputfile = ''

def main(argv):
    try:
        opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
    except getopt.GetoptError:
        print('errata-convert.py -i <inputfile> -o <outputfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('errata-convert.py -i <inputfile> -o <outputfile>')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg
    
    print("Starting the process")
    print("#######################################################")
    
    xmldoc = minidom.parse(inputfile)
    years = xmldoc.getElementsByTagName('year')
    
    f = open(outputfile, "w")
    
    f.write("# Sort errata notices by year, month and day\n")
    f.write("# $FreeBSD$\n")
    f.write("\n")

    for year in years:
        yearNumber = year.getElementsByTagName("name")[0]
        months = year.getElementsByTagName("month")
        
        for month in months:
            monthNumber = month.getElementsByTagName("name")[0]
            days = month.getElementsByTagName("day")
            
            for day in days:
                dayNumber = day.getElementsByTagName("name")[0]
                notices = day.getElementsByTagName("notice")
                
                for notice in notices:
                    f.write("[[notices]]\n")
                    noticeName = notice.getElementsByTagName("name")[0]
                    date = yearNumber.firstChild.nodeValue+"-"+monthNumber.firstChild.nodeValue+"-"+dayNumber.firstChild.nodeValue;
                    dateFormat = datetime.datetime.strptime(date, "%Y-%m-%d").date()
                    f.write("name = \"{}\"\n".format(noticeName.firstChild.nodeValue))
                    f.write("date = \"{}\"\n".format(dateFormat))
                    f.write("\n")
    
    f.close()

    print("Process finished");
    
if __name__ == "__main__":
    main(sys.argv[1:])
