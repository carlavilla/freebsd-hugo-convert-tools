#!/usr/bin/env python3

import sys, getopt, html, datetime
from dateutil import parser
from xml.dom import minidom

# Global variables
# Yes, yes, I know, I can use an [][]... but remember, the idea it's to only execute this program only ONCE time
JanList = []
FebList = []
MarList = []
AprList = []
MayList = []
JunList = []
JulList = []
AugList = []
SepList = []
OctList = []
NovList = []
DecList = []

year = 2020 # only to prevent some bugs...
inputfile = ''
outputfile = ''

class Event:
    def __init__(self, id, name, url, startDate, endDate, countryCode, country, city, site, state, address, description):
        self.id = id
        self.name = name
        self.url = url
        self.startDate = startDate
        self.endDate = endDate
        self.countryCode = countryCode
        self.country = country
        self.city = city
        self.site = site
        self.state = state
        self.address = address
        self.description = description

def handleAttribute(nodeList):
    rc = []
    for node in nodeList:
        rc.append(node.attributes['code'].value)
    return ''.join(rc)

def handleLocation(nodeList):
    rc = []
    for node in nodeList:
        if node.nodeType == node.ELEMENT_NODE:
            if node.childNodes:
                rc.append(node.childNodes[0].data.replace("\t", " ").replace("\n", "").replace("\"", "\\\""))
    return ''.join(rc)
        
def handleDate(nodeList):
    rc = []
    for node in nodeList:
        if node.nodeType == node.ELEMENT_NODE:
            rc.append(node.childNodes[0].data)
            if rc.count('-') < 2:
                rc.append("-")
    return ''.join(rc)

def getText(nodelist):
    rc = []
    
    for i, node in enumerate(nodelist):
        if node.nodeType == node.TEXT_NODE:
            rc.append(node.data.replace("\t", " ").replace("\n", "").replace("\"", "\\\""))
    return ''.join(rc)

def handleDescription(nodeList):
    rc = []

    for i, node in enumerate(nodeList):
        if node.nodeType == node.TEXT_NODE:
            data = ''
            data = node.data.replace("\n", "").replace("\"", "\\\"")
            data = data.split()
            rc.append(' '.join(data))
        if node.nodeType == node.ELEMENT_NODE:
            if node.tagName == "a":
                url = ''
                url = ' <a href=\\"' + node.getAttribute('href').replace("\t", "").replace("\n", "") + '\\">' + getText(node.childNodes) + '</a>'
                url = url.rstrip()
                rc.append(''.join(url))
                if (i < len(nodeList)):
                    rc.append(' ')
            if node.tagName == "p":
                p = ''
                p = ' <p>' + getText(node.childNodes) + '</p>'
                p = p.split()
                rc.append(' '.join(p))
                if (i < len(nodeList)):
                    rc.append(' ')
            if node.tagName == "em":
                em = ''
                em = ' <em>' + getText(node.childNodes) + '</em> '
                rc.append(''.join(em))
            if node.tagName == "b":
                strong = ''
                strong = ' <strong>' + getText(node.childNodes) + '</strong> '
                rc.append(''.join(strong))
            if node.tagName == "br":
                br = ''
                br = '<br />'
                rc.append(''.join(br))
    return ''.join(rc).strip().replace("&", "&amp;").replace(" .", ".").replace("</a> ,", "</a>,").replace("</a> .", "</a>.").replace("( <a", "(<a").replace("</a> )", "</a>)").replace(".<p>", ". <p>").replace("</em> ," ,"</em>,").replace("near<strong>", "near <strong>").replace("</strong> ,", "</strong>,").replace("( <strong>", "(<strong>")

def loadMonth(f, monthList, monthNumber):

    for i, event in enumerate(monthList):

        f.write("[[events]]\n")
        f.write("id = \"{}\"\n".format(event.id))
        f.write("name = \"{}\"\n".format(event.name))
        
        if event.url:
            f.write("url = \"{}\"\n".format(event.url))
        
        start = parser.parse(event.startDate)
        f.write("startDate = \"{}\"\n".format(start.strftime("%Y-%m-%d")))
        
        end = parser.parse(event.endDate)
        f.write("endDate = \"{}\"\n".format(end.strftime("%Y-%m-%d")))
        
        if event.countryCode:
            f.write("countryCode = \"{}\"\n".format(event.countryCode))
        if event.country:
            f.write("country = \"{}\"\n".format(event.country))
        if event.city:
            f.write("city = \"{}\"\n".format(event.city))
        if event.site:
            f.write("site = \"{}\"\n".format(event.site))
        if event.state:
            f.write("state = \"{}\"\n".format(event.state))
        if event.address:
            f.write("address = \"{}\"\n".format(event.address))

        f.write("description = \"{}\"\n".format(event.description))
        f.write("\n")

def main(argv):
    try:
        opts, args = getopt.getopt(argv,"hy:i:o:",["year=","ifile=","ofile="])
    except getopt.GetoptError:
        print('events-convert.py -y <year> -i <inputfile> -o <outputfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('events-convert.py -y <year> -i <inputfile> -o <outputfile>')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg
        elif opt in ("-y", "--year"):
            year = arg
    
    print("Starting the process")
    print("#######################################################")

    fin = open(inputfile, "rt")
    contents = fin.read()
    contents = contents.replace("&os;", "FreeBSD").replace("&mdash;", "—").replace("&ouml;", "ö").replace("&auml;", "ä").replace("&lt;", "\"").replace("&gt;", "\"").replace("&#38;", "&").replace("&eacute;", "é").replace("&aacute;", "á").replace("&nbsp;", " ").replace("&deg;", "°").replace("&#176;", "°").replace("&base;", "https://www.FreeBSD.org").replace("&uuml;", "ü").replace("&oacute;", "ó").replace("&ecirc;", "ê").replace("&#252;", "ü").replace("&#228;", "ä").replace("&#223;", "ß").replace("&#233;", "é").replace("&#243;", "ó").replace("&#246;", "ö")
    fin.close()

    fin = open(inputfile, "wt")
    fin.write(contents)
    fin.close()

    f1 = open(inputfile, "r")
    
    xmldoc = minidom.parse(inputfile)
    events = xmldoc.getElementsByTagName('event')

    for event in events:
        
        if 'id' in event.attributes:
            id = event.attributes['id'].value
        else:
            id = None

        name = event.getElementsByTagName("name")[0]

        if len(event.getElementsByTagName("url")) >= 1:
            url = event.getElementsByTagName("url")[0]
        else:
            url = None

        startDate = event.getElementsByTagName("startdate")[0]
        endDate = event.getElementsByTagName("enddate")[0]

        if len(event.getElementsByTagName("location")) >= 1:
            location = event.getElementsByTagName("location")[0]
        else:
            location = None

        description = event.getElementsByTagName("description")[0]

        entryObject = Event(id, getText(name.childNodes),
                                getText(url.childNodes if url is not None else ""),
                                handleDate(startDate.childNodes),
                                handleDate(endDate.childNodes),
                                handleAttribute(location.getElementsByTagName("country") if location is not None else []),
                                handleLocation(location.getElementsByTagName("country") if location is not None else []),
                                handleLocation(location.getElementsByTagName("city") if location is not None else []),
                                handleLocation(location.getElementsByTagName("site") if location is not None else []),
                                handleLocation(location.getElementsByTagName("state") if location is not None else []),
                                handleLocation(location.getElementsByTagName("address") if location is not None else []),
                                handleDescription(description.childNodes))
        
        dateComplete = datetime.datetime.strptime(entryObject.startDate, "%Y-%m-%d")
        month = '{:02d}'.format(dateComplete.month)

        if month == '01':
            JanList.append(entryObject)
        elif month == '02':
            FebList.append(entryObject)
        elif month == '03':
            MarList.append(entryObject)
        elif month == '04':
            AprList.append(entryObject)
        elif month == '05':
            MayList.append(entryObject)
        elif month == '06':
            JunList.append(entryObject)
        elif month == '07':
            JulList.append(entryObject)
        elif month == '08':
            AugList.append(entryObject)
        elif month == '09':
            SepList.append(entryObject)
        elif month == '10':
            OctList.append(entryObject)
        elif month == '11':
            NovList.append(entryObject)
        elif month == '12':
            DecList.append(entryObject)

    f = open(outputfile, "w")

    f.write("# Sort events by start date, with more recent events earlier in the file\n")
    f.write("# $FreeBSD$\n")
    f.write("year = {}\n".format(year))
    f.write("\n")
    
    print("Loading events...")
    
    # Don't judge me...

    if len(JanList) >= 1:
        loadMonth(f, JanList, "01")
    if len(FebList) >= 1:
        loadMonth(f, FebList, "02")
    if len(MarList) >= 1:
        loadMonth(f, MarList, "03")
    if len(AprList) >= 1:
        loadMonth(f, AprList, "04")
    if len(MayList) >= 1:
        loadMonth(f, MayList, "05")
    if len(JunList) >= 1:
        loadMonth(f, JunList, "06")
    if len(JulList) >= 1:
        loadMonth(f, JulList, "07")
    if len(AugList) >= 1:
        loadMonth(f, AugList, "08")
    if len(SepList) >= 1:
        loadMonth(f, SepList, "09")
    if len(OctList) >= 1:
        loadMonth(f, OctList, "10")
    if len(NovList) >= 1:
        loadMonth(f, NovList, "11")
    if len(DecList) >= 1:
        loadMonth(f, DecList, "12")
    
    f.close()
    print("#######################################################")
    print("Process finished");
    
if __name__ == "__main__":
    main(sys.argv[1:])
