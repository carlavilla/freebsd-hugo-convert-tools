#!/usr/bin/env python3

import sys, getopt, html
import re
from xml.dom import minidom

# Global variables
inputfile = ''
outputfile = ''

def getText(nodelist):
    rc = []
    
    for i, node in enumerate(nodelist):
        if node.nodeType == node.TEXT_NODE:
            rc.append(node.data.replace("\t", " ").replace("\n", "").replace("\"", "\\\""))
    return ''.join(rc)

def handleDescription(nodeList):
    rc = []

    for i, node in enumerate(nodeList):
        if node.nodeType == node.TEXT_NODE:
            data = ''
            data = node.data.replace("\n", "").replace("\"", "\\\"")
            data = data.split()
            rc.append(' '.join(data))
        if node.nodeType == node.ELEMENT_NODE:
            if node.tagName == "a":
                url = ''
                url = ' <a href=\\"' + node.getAttribute('href').replace("\t", "").replace("\n", "") + '\\">' + getText(node.childNodes) + '</a>'
                url = url.rstrip()
                url = re.sub(' +',' ',url)
                rc.append(''.join(url))
                if (i < len(nodeList)):
                    rc.append(' ')
            if node.tagName == "em":
                em = ''
                em = ' <em>' + getText(node.childNodes) + '</em> '
                rc.append(''.join(em))
            if node.tagName == "sup":
                sup = ''
                sup = ' <sup>' + getText(node.childNodes) + '</sup> '
                rc.append(''.join(sup))
            if node.tagName == "code":
                code = ''
                code = ' <code>' + getText(node.childNodes) + '</code> '
                rc.append(''.join(code))
    return ''.join(rc).strip().replace("</a> .", "</a>.").replace("\"> ", "\">").replace("( <a", "(<a").replace("</a> )", "</a>)")

def main(argv):
    try:
        opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
    except getopt.GetoptError:
        print('usergroups-convert.py -i <inputfile> -o <outputfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('usergroups-convert.py -i <inputfile> -o <outputfile>')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg
    
    print("Starting the process")
    print("#######################################################")
    
    fin = open(inputfile, "rt")
    contents = fin.read()
    contents = contents.replace("&uuml;", "ü").replace("&eacute;", "é").replace("&os;", "FreeBSD").replace("North America", "NorthAmerica").replace("South America", "SouthAmerica")
    fin.close()

    fin = open(inputfile, "wt")
    fin.write(contents)
    fin.close()
    
    f1 = open(inputfile, "r")
    lines = f1.readlines() 

    f = open(outputfile, "w")
    
    f.write("# The country codes are precise ISO3166 codes from https://www.iso.org/obp/ui/#search/code/\n")
    f.write("# $FreeBSD$\n")
    
    print("Loading usergroups...")
    
    xmldoc = minidom.parse(inputfile)
    continents = xmldoc.getElementsByTagName('continent')
    
    for continent in continents:
        continentName = continent.attributes['name'].value

        countries = continent.getElementsByTagName('country')
        for country in countries:
            
            if 'code' in country.attributes:
                countryCode = country.attributes['code'].value
            else:
                countryCode = None
            
            entries = country.getElementsByTagName('entry')
            for entry in entries:

                f.write("\n[[{}]]\n".format(continentName))
                f.write("id = \"{}\"\n".format(entry.attributes['id'].value))
                f.write("name = \"{}\"\n".format(getText(entry.getElementsByTagName("name")[0].childNodes)))
                f.write("url = \"{}\"\n".format(getText(entry.getElementsByTagName("url")[0].childNodes)))
                
                if countryCode:
                    f.write("countryCode = \"{}\"\n".format(countryCode))
                
                f.write("description = \"{}\"\n".format(handleDescription(entry.getElementsByTagName("description")[0].childNodes)))

if __name__ == "__main__":
    main(sys.argv[1:])
