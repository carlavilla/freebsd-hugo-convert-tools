#!/usr/bin/env python3

import sys, getopt, html, datetime
from dateutil import parser
from xml.dom import minidom

# Global variables
textList = []
inputfile = ''
outputfile = ''
        
def getText(nodelist):
    rc = []
    
    for i, node in enumerate(nodelist):
        if node.nodeType == node.TEXT_NODE:
            rc.append(node.data.replace("\t", " ").replace("\n", "").replace("\"", "\\\""))
    return ''.join(rc)

def handleDescription(nodeList):
    rc = []

    for i, node in enumerate(nodeList):
        if node.nodeType == node.TEXT_NODE:
            data = ''
            data = node.data.replace("\n", "").replace("\"", "\\\"")
            data = data.split()
            rc.append(' '.join(data))
        if node.nodeType == node.ELEMENT_NODE:
            if node.tagName == "a":
                url = ''
                url = ' <a href=\\"' + node.getAttribute('href').replace("\t", "").replace("\n", "") + '\\">' + getText(node.childNodes) + '</a>'
                url = url.rstrip()
                rc.append(''.join(url))
                if (i < len(nodeList)):
                    rc.append(' ')
            if node.tagName == "p":
                p = ''
                p = ' <p>' + getText(node.childNodes) + '</p>'
                p = p.split()
                rc.append(' '.join(p))
                if (i < len(nodeList)):
                    rc.append(' ')


    return ''.join(rc).strip().replace(" .", ".").replace("</a> ,", "</a>,").replace("</a> .", "</a>.").replace("( <a", "(<a").replace("</a> )", "</a>)").replace(".<p>", ". <p>")

def main(argv):
    try:
        opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
    except getopt.GetoptError:
        print('press-convert.py -i <inputfile> -o <outputfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('press-convert.py -i <inputfile> -o <outputfile>')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg
    
    print("Starting the process")
    print("#######################################################")
    
    f1 = open(inputfile, "r")
    
    contents = html.unescape(f1.read())
    
    contents = contents.replace("&os;", "FreeBSD").replace("&mdash;", "—").replace("&quot;", "\"")
    
    xmldoc = minidom.parse(inputfile)
    years = xmldoc.getElementsByTagName('year')
    
    f = open(outputfile, "w")

    f.write("# Sort the entries by name\n")
    f.write("# $FreeBSD$\n")
    f.write("\n")

    for year in years:
        yearNumber = year.getElementsByTagName("name")[0]
        months = year.getElementsByTagName("month")
        
        for month in months:
            monthNumber = month.getElementsByTagName("name")[0]            
            stories = month.getElementsByTagName("story")
            
            for story in stories:                
                name = story.getElementsByTagName("name")[0]
                url = story.getElementsByTagName("url")[0]
                siteName = story.getElementsByTagName("site-name")[0]
                siteUrl = story.getElementsByTagName("site-url")[0]
                date = story.getElementsByTagName("date")[0]
                description = story.getElementsByTagName("p")[0]
                
                f.write("[[press]]\n")
                f.write("name = \"{}\"\n".format(handleDescription(name.childNodes)))
                f.write("url = \"{}\"\n".format(getText(url.childNodes)))
                f.write("siteName = \"{}\"\n".format(getText(siteName.childNodes)))
                f.write("siteUrl = \"{}\"\n".format(getText(siteUrl.childNodes)))
                d = parser.parse(getText(date.childNodes))
                f.write("date = \"{}\"\n".format(d.strftime("%Y-%m-%d")))
                if len(story.getElementsByTagName("author")) > 0:
                    author = story.getElementsByTagName("author")[0]
                    f.write("author = \"{}\"\n".format(getText(author.childNodes)))
                f.write("description = \"{}\"\n".format(handleDescription(description.childNodes)))

                f.write("\n")
    
    f.close()

    print("Process finished");
    
if __name__ == "__main__":
    main(sys.argv[1:])
