#!/usr/bin/env python3

import sys, getopt, re

# Global variables
inputfile = ''
outputfile = ''

def main(argv):
    try:
        opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
    except getopt.GetoptError:
        print('mailing-lists-convert.py -i <inputfile> -o <outputfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('mailing-lists-convert.py -i <inputfile> -o <outputfile>')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg
    
    print("Starting the process")
    print("#######################################################")
        
    f1 = open(inputfile, "r")
    lines = f1.readlines() 

    f = open(outputfile, "w")

    f.write("#\n")
    f.write("# Names of FreeBSD mailing lists and related software.\n")
    f.write("# $FreeBSD$\n")
    f.write("#\n")
    f.write("\n")
    
    counter = 0
    
    code = ""
    description = ""
    URL = ""
    
    for line in lines:
        if "<!ENTITY" in line and "url" in line and "<link" not in line:
            #URL
            URL = re.search('"(.+?)"', line)
            if URL:
                URL = URL.group(1)
                print("URL " + URL)
                
        if "<!ENTITY" in line and "name" not in line:
            #description            
            description = re.search('>(.+?)</link>', line)
            if description:
                description = description.group(1)
                print("description " + description)

        if "ENTITY" in line and "name" in line:
            # Code
            code = re.search('>(.+?)</', line)
            if code:
                code = code.group(1)
                print("code " + code)
        counter += 1
        
        remainder =  counter % 4

        if remainder == 0:
            f.write("[{}]\n".format(code))
            f.write("description = \"{}\"\n".format(description))
            f.write("URL = \"{}\"\n".format(URL))
            f.write("\n")
            
            code = ''
            description = ''
            URL = ''
            
    f.close()

    print("Process finished");
    
if __name__ == "__main__":
    main(sys.argv[1:])
 
