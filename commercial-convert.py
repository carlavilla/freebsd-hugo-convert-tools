#!/usr/bin/env python3

import sys, getopt, html
from xml.dom import minidom

# Global variables
textList = []
label = ''
inputfile = ''
outputfile = ''

class Entry:
    def __init__(self, id, category, name, url, description):
        self.id = id
        self.category = category
        self.name = name
        self.url = url
        self.description = description

def getText(nodelist):
    rc = []
    
    for i, node in enumerate(nodelist):
        if node.nodeType == node.TEXT_NODE:
            rc.append(node.data.replace("\t", " ").replace("\n", "").replace("\"", "\\\""))
        if node.nodeType == node.ELEMENT_NODE:
            if node.tagName == "a":
                url = ''
                url = ' <a href=\\"' + node.getAttribute('href').replace("\t", "").replace("\n", "") + '\\">' + getText(node.childNodes) + '</a>'
                url = url.rstrip()
                rc.append(''.join(url))
    return ''.join(rc)

def handleDescription(nodeList):
    rc = []

    for i, node in enumerate(nodeList):
        if node.nodeType == node.TEXT_NODE:
            data = ''
            data = node.data.replace("\n", "").replace("\"", "\\\"")
            data = data.split()
            rc.append(' '.join(data))
        if node.nodeType == node.ELEMENT_NODE:
            if node.tagName == "a":
                url = ''
                url = ' <a href=\\"' + node.getAttribute('href').replace("\t", "").replace("\n", "") + '\\">' + getText(node.childNodes) + '</a>'
                url = url.rstrip()
                rc.append(''.join(url))
                if (i < len(nodeList)):
                    rc.append(' ')
            if node.tagName == "p":
                p = ''
                p = ' <p>' + getText(node.childNodes) + '</p>'
                p = p.split()
                rc.append(' '.join(p))
                if (i < len(nodeList)):
                    rc.append(' ')
            if node.tagName == "em":
                em = ''
                em = ' <em>' + getText(node.childNodes) + '</em> '
                rc.append(''.join(em))
            if node.tagName == "b":
                strong = ''
                strong = ' <strong>' + getText(node.childNodes) + '</strong> '
                rc.append(''.join(strong))
            if node.tagName == "br":
                br = ''
                br = '<br />'
                rc.append(''.join(br))
    return ''.join(rc).strip().replace(" .", ".").replace("</a> ,", "</a>,").replace("</a> .", "</a>.").replace("( <a", "(<a").replace("</a> )", "</a>)").replace(".<p>", ". <p>").replace("</em> ," ,"</em>,").replace("near<strong>", "near <strong>").replace("</strong> ,", "</strong>,").replace("( <strong>", "(<strong>")

def main(argv):
    try:
        opts, args = getopt.getopt(argv,"hl:i:o:",["label=","ifile=","ofile="])
    except getopt.GetoptError:
        print('commercial-convert.py -l <label> -i <inputfile> -o <outputfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('commercial-convert.py -l <label> -i <inputfile> -o <outputfile>')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg
        elif opt in ("-l", "--label"):
            label = arg
    
    print("Starting the process")
    print("#######################################################")
    
    f1 = open(inputfile, "r")
    
    contents = html.unescape(f1.read())
    
    contents = contents.replace("&os;", "FreeBSD").replace("&mdash;", "—").replace("&ouml;", "ö")

    xmldoc = minidom.parse(inputfile)
    entries = xmldoc.getElementsByTagName('entry')

    for entry in entries:
        id = entry.attributes['id'].value
        if 'category' in entry.attributes:
            category = entry.attributes['category'].value
        else:
            category = None
        name = entry.getElementsByTagName("name")[0]
        url = entry.getElementsByTagName("url")[0]
        description = entry.getElementsByTagName("description")[0]

        entryObject = Entry(id, None if category is None else category, getText(name.childNodes), getText(url.childNodes), handleDescription(description.childNodes))
        
        textList.append(entryObject)

    f = open(outputfile, "w")

    f.write("# Sort the entries by name\n")
    f.write("# $FreeBSD$\n")
    f.write("\n")
    
    print("Loading vendors...")
    
    for i, category in enumerate(textList):
        f.write("[[{}]]\n".format(label))
        f.write("id = \"{}\"\n".format(category.id.replace(" ", "-")))
        if category.category is not None:
            f.write("category = \"{}\"\n".format(category.category))
        f.write("name = \"{}\"\n".format(category.name))
        f.write("url = \"{}\"\n".format(category.url))
        f.write("description = \"{}\"\n".format(category.description))
        f.write("\n")
    f.close()
    print("#######################################################")
    print("Process finished");
    
if __name__ == "__main__":
    main(sys.argv[1:])
 
