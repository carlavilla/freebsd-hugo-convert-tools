#!/usr/bin/env python3

import sys, getopt, re
import os,glob

# Global variables
folder = ''

def processPGPKeysEntities(content):
    content = re.sub('<sect2 xmlns=\"http://docbook.org/ns/docbook\" xml:id=\"pgpkey-[A-Za-z0-9_-]*\">', '', content)
    content = content.replace("</sect2>", "")

    content = content.replace("<title>&a.", "=== {{< get-authors \"")
    content = content.replace(".email;</title>", "\" \"both\" >}}")

    content = content.replace("&pgpkey.", "include::static/pgpkeys/")
    content = content.replace(";", ".key[filetype=adoc]")

    content = re.sub(r'\n\s*\n', '\n\n', content)

    return content

def main(argv):
    try:
        opts, args = getopt.getopt(argv,"hi:",["folder="])
    except getopt.GetoptError:
        print('pgpkeys-processing.py -i <folder>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('pgpkeys-processing.py -i <folder>')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            folder = arg
    
    print("Starting the process")
    print("#######################################################")

    # Prepare all the *.key
    for filename in glob.glob(os.path.join(folder, '*.key')):
        with open(filename, 'r+') as f:
            text = f.read()

            text = text.replace("<!-- $FreeBSD:", "////\n$FreeBSD:")
            text = text.replace("$ -->", "$")
            text = text.replace("<!--", "")
            text = text.replace("-->", "////\n")
            text = text.replace("<programlisting xmlns=\"http://docbook.org/ns/docbook\" role=\"pgpfingerprint\"><![CDATA[", "[.literal-block-margin]\n....")
            text = text.replace("<programlisting role=\"pgpfingerprint\"><![CDATA[", "[.literal-block-margin]\n....")
            text = text.replace("<programlisting role=\"pgpkey\"><![CDATA[", "[.literal-block-margin]\n....")
            text = text.replace("]]></programlisting>", "....\n")
            text = text.replace("<programlisting xmlns=\"http://docbook.org/ns/docbook\" role=\"pgpkey\"><![CDATA[", "[.literal-block-margin]\n....")
            text = text.replace("]]></programlisting>", "....")

            f.seek(0)
            f.write(text)
            f.truncate()
            f.close()
    
    # Prepare pgpkeys-developers.xml
    developers = open(folder + "/pgpkeys-developers.xml", "r+")
    developersContent = developers.read()
    
    developers.seek(0)
    developers.write(processPGPKeysEntities(developersContent))
    developers.truncate()
    developers.close()

    # Prepare pgpkeys-core.xml
    core = open(folder + "/pgpkeys-core.xml", "r+")
    coreContent = core.read()
    
    core.seek(0)
    core.write(processPGPKeysEntities(coreContent))
    core.truncate()
    core.close()

    # Prepare pgpkeys-officers.xml
    officers = open(folder + "/pgpkeys-officers.xml", "r+")
    officersContent = officers.read()
    
    officers.seek(0)
    officers.write(processPGPKeysEntities(officersContent))
    officers.truncate()
    officers.close()

    # Prepare pgpkeys-other.xml
    other = open(folder + "/pgpkeys-other.xml", "r+")
    otherContent = other.read()
    
    other.seek(0)
    other.write(processPGPKeysEntities(otherContent))
    other.truncate()
    other.close()

    print("Process finished")
    
if __name__ == "__main__":
    main(sys.argv[1:])
