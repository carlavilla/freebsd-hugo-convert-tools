#!/usr/bin/env python3

import sys, getopt, re

# Global variables
inputfile = ''
outputfile = ''

def main(argv):
    try:
        opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
    except getopt.GetoptError:
        print('authors-convert.py -i <inputfile> -o <outputfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('authors-convert.py -i <inputfile> -o <outputfile>')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg
    
    print("Starting the process")
    print("#######################################################")
    
    fin = open(inputfile, "rt")
    contents = fin.read()
    contents = contents.replace("&ouml;", "ö").replace("&auml;", "ä").replace("&aacute;", "á").replace("&uuml;", "ü").replace("&ecirc;", "ê").replace("&eacute;", "é").replace("&iacute;", "í").replace("&lstrok;", "ł").replace("&szlig;", "ß").replace("&ldquo;", "\"").replace("&rdquo;", "\"").replace("&oslash;", "ø").replace("&#382;", "ž").replace("&#231;", "ç").replace("&egrave;", "è")
    fin.close()

    fin = open(inputfile, "wt")
    fin.write(contents)
    fin.close()
    
    f1 = open(inputfile, "r")
    lines = f1.readlines() 

    f = open(outputfile, "w")

    f.write("#\n")
    f.write("#     Names and email address of contributing authors and committers.\n")
    f.write("#     Entity names for committers should be the same as their login names on\n")
    f.write("#     freefall.FreeBSD.org.\n")
    f.write("#\n")
    f.write("#     Use these entities when referencing people.\n")
    f.write("#\n")
    f.write("#     Please keep this list in alphabetical order by entity names.\n")
    f.write("#\n")
    f.write("#     IMPORTANT:  If you delete names from this file you *must* ensure that\n")
    f.write("#                 all references to them have been removed from the handbook's\n")
    f.write("#                 translations.  If they haven't then you *will* break the\n")
    f.write("#                 builds for the other languages, and we will poke fun of you\n")
    f.write("#                 in public.\n")
    f.write("# $FreeBSD$\n")
    f.write("#\n")
    f.write("\n")
    
    for line in lines:
        if "ENTITY" in line and "email" not in line:
            #Code
            code = re.search('a.(.+?) ', line)
            if code:
                code = code.group(1)
                f.write("[{}]\n".format(code))
            
            # Name
            name = re.search('"(.+?)"', line)
            if name:
                name = name.group(1)
                f.write("name = \"{}\"\n".format(name))

        if "ENTITY" in line and "email" in line:
            # Email
            email = re.search('<email xmlns=\'http://docbook.org/ns/docbook\'>(.+?)</email>', line)
            if email:
                email = email.group(1)
                f.write("email = \"{}\"\n".format(email))
            f.write("\n")
    f.close()

    print("Process finished");
    
if __name__ == "__main__":
    main(sys.argv[1:])
 
