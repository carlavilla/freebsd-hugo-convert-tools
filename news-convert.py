#!/usr/bin/env python3

import sys, getopt, html, datetime
from xml.dom import minidom

# Global variables
textList = []
inputfile = ''
outputfile = ''

def getText(nodelist):
    rc = []
    
    for i, node in enumerate(nodelist):
        if node.nodeType == node.TEXT_NODE:
            rc.append(node.data.replace("\t", " ").replace("\n", "").replace("\"", "\\\""))
        if node.nodeType == node.ELEMENT_NODE:
            if node.tagName == "a":
                url = ''
                url = ' <a href=\\"' + node.getAttribute('href').replace("\t", "").replace("\n", "") + '\\">' + getText(node.childNodes) + '</a>'
                url = url.rstrip()
                rc.append(''.join(url))
    return ''.join(rc)

def handleDescription(nodeList, useParagraph):
    rc = []

    for i, node in enumerate(nodeList):
        if node.nodeType == node.TEXT_NODE:
            data = ''
            data = node.data.replace("\n", "").replace("\"", "\\\"")
            data = data.split()
            rc.append(' '.join(data))
        if node.nodeType == node.ELEMENT_NODE:
            if node.tagName == "a":
                url = ''
                url = ' <a href=\\"' + node.getAttribute('href').replace("\t", "").replace("\n", "") + '\\">' + getText(node.childNodes) + '</a>'
                url = url.rstrip()
                rc.append(''.join(url))
                if (i < len(nodeList)):
                    rc.append(' ')
            if node.tagName == "p":
                p = ''
                if useParagraph:
                    p = ' <p>' + getText(node.childNodes) + '</p>'
                else:
                    p = getText(node.childNodes)
                p = p.split()
                rc.append(' '.join(p))
                if (i < len(nodeList)):
                    rc.append(' ')
    return ''.join(rc).strip().replace(" .", ".").replace("</a> ,", "</a>,").replace("</a> .", "</a>.").replace("( <a", "(<a").replace("</a> )", "</a>)").replace("<p> <a", "<p><a")

def main(argv):
    try:
        opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
    except getopt.GetoptError:
        print('news-convert.py -i <inputfile> -o <outputfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('news-convert.py -i <inputfile> -o <outputfile>')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg
    
    print("Starting the process")
    print("#######################################################")
    
    f1 = open(inputfile, "r")
    
    contents = html.unescape(f1.read())
    
    contents = contents.replace("&os;", "FreeBSD ").replace("&enbase;", "https://www.freebsd.org").replace("&lists.pkg;", "https://lists.freebsd.org/pipermail/freebsd-pkg").replace("&lists.stable;", "https://lists.freebsd.org/pipermail/freebsd-stable").replace("&url.doc.base-en;", "https://www.freebsd.org/doc/en_US.ISO8859-1").replace("&lists.current;", "https://lists.freebsd.org/pipermail/freebsd-current").replace("&lists.ports-announce;", "https://lists.freebsd.org/pipermail/freebsd-ports-announce").replace("&lists.announce;", "https://lists.freebsd.org/pipermail/freebsd-announce")
    
    xmldoc = minidom.parseString(contents)
    years = xmldoc.getElementsByTagName('year')
    
    f = open(outputfile, "w")
    
    f.write("# Sort news by year, month and day\n")
    f.write("# $FreeBSD$\n")
    f.write("\n")

    for year in years:
        yearNumber = year.getElementsByTagName("name")[0]
        months = year.getElementsByTagName("month")
        
        for month in months:
            monthNumber = month.getElementsByTagName("name")[0]
            days = month.getElementsByTagName("day")
            
            for day in days:
                dayNumber = day.getElementsByTagName("name")[0]
                events = day.getElementsByTagName("event")
                
                for event in events:
                    
                    f.write("[[news]]\n")
                    date = yearNumber.firstChild.nodeValue+"-"+monthNumber.firstChild.nodeValue+"-"+dayNumber.firstChild.nodeValue;
                    dateFormat = datetime.datetime.strptime(date, "%Y-%m-%d").date()
                    f.write("date = \"{}\"\n".format(dateFormat))
                    
                    if event.getElementsByTagName("title"):
                        title = event.getElementsByTagName("title")[0]
                        if title:
                            f.write("title = \"{}\"\n".format(handleDescription(title.childNodes, False)))
                    
                    useParagraph = True if event.getElementsByTagName("title") else False
                    description = handleDescription(event.childNodes, useParagraph)
                    
                    f.write("description = \"{}\"\n".format(description))
                    f.write("\n")
    
    f.close()

    print("Process finished");
    
if __name__ == "__main__":
    main(sys.argv[1:])
 
