#!/usr/bin/env python3

import sys, getopt, re

# Global variables
name = ''
language = ''
inputfile = ''
outputfile = ''
documentFile = ''

def main(argv):
    try:
        opts, args = getopt.getopt(argv,"hn:l:i:o:",["name=","language=","ifile=","ofile="])
    except getopt.GetoptError:
        print('articles-books-post-processing.py -n <name> -l <language> -i <inputfile> -o <outputfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('articles-books-post-processing.py -n <name> -l <language> -i <inputfile> -o <outputfile>')
            sys.exit()
        elif opt in ("-n", "--name"):
            name = arg
        elif opt in ("-l", "--language"):
            language = arg
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg
    
    print("Starting the process")
    print("#######################################################")

    # Remove two consecutive empty lines
    with open(inputfile, 'rt') as fp:
        documentFile = fp.read()
        documentFile = re.sub(r'\n\s*\n', '\n\n', documentFile)
        documentFile = documentFile.replace("[.acronym]#ACL#", "ACL")
        documentFile = documentFile.replace("McKusick.http://", "McKusick. http://")
        documentFile = documentFile.replace("[.acronym]#HTTP#[.acronym]#URI#", "[.acronym]#HTTP# [.acronym]#URI#")
        documentFile = documentFile.replace(" as the name,", "as the name,")
        documentFile = documentFile.replace("( {{", "({{")
        documentFile = documentFile.replace("{ldquo}", "\"")
        documentFile = documentFile.replace("{rdquo}", "\"")
        documentFile = documentFile.replace("{ng.misc}", "news:comp.unix.bsd.freebsd.misc")
        documentFile = documentFile.replace("@@URL_RELPREFIX@@/index.html", "https://www.FreeBSD.org")
        documentFile = documentFile.replace("@@URL_RELPREFIX@@", "https://www.FreeBSD.org")
        documentFile = documentFile.replace("{{< articles \"contributors.en\" \"Contributors to FreeBSD\" >}}", "{{< articles \"contributors\" \"Contributors to FreeBSD\" >}}")
        documentFile = documentFile.replace("https://www.FreeBSD.org/index/", "https://www.FreeBSD.org/")
        documentFile = documentFile.replace("https://www.FreeBSD.org/search/search.html#mailinglists", "https://www.FreeBSD.org/search/#mailinglists")
        documentFile = documentFile.replace("[.application]#X11#[.filename]#fonts.dir#", "[.application]#X11# [.filename]#fonts.dir#")

        if (name == 'building-products'):
            documentFile = documentFile.replace("https://www.FreeBSD.org/support/bugreports.html", "https://www.FreeBSD.org/support/bugreports/")
            
            if (language == 'en'):
                documentFile = documentFile.replace("<<fig-freebsd-branches>>", "<<fig-freebsd-branches, FreeBSD Release Branches>>")
                documentFile = documentFile.replace("<<fig-change-log>>", "<<fig-change-log, A sample change log entry>>")
                documentFile = documentFile.replace("<<fig-svn-blame>>", "<<fig-svn-blame, An annotated source listing generated using [.command]#svn blame#>>")
            if (language == 'fr'):
                documentFile = documentFile.replace("<<fig-freebsd-branches>>", "<<fig-freebsd-branches, Les branches FreeBSD>>")
                documentFile = documentFile.replace("<<fig-change-log>>", "<<fig-change-log, Un example de rapport de modification>>")
                documentFile = documentFile.replace("<<fig-cvs-annotate>>", "<<fig-cvs-annotate, Un listing annoté généré par [.command]#cvs annotate#>>")
            if (language == 'br'):
                documentFile = documentFile.replace("<<fig-freebsd-branches>>", "<<fig-freebsd-branches, Release Branches do FreeBSD>>")
                documentFile = documentFile.replace("<<fig-change-log>>", "<<fig-change-log, Um exemplo de entrada no log de alteração>>")
                documentFile = documentFile.replace("<<fig-svn-blame>>", "<<fig-svn-blame, Código fonte exibindo a listagem de anotações gerada utilizando o [.command]#svn blame#>>")

        if (name == 'contributing'):
            documentFile = documentFile.replace("[.filename]#Makefile#s", "[.filename]#Makefile#'s")
            documentFile = documentFile.replace("{{< manpage \"uname\" \"1\" >}}[.command]# -a#", "{{< manpage \"uname\" \"1\" >}} [.command]#-a#")
            documentFile = documentFile.replace("{{< manpage \"diff\" \"1\" >}}`-ruN`", "{{< manpage \"diff\" \"1\" >}} `-ruN`")

        if (name == 'committers-guide'):
            documentFile = documentFile.replace("Open[.acronym]#PGP#", "OpenPGP")
            documentFile = documentFile.replace("[.fqdomainname]#freefall.FreeBSD.org#", "`freefall.FreeBSD.org`")
            documentFile = documentFile.replace("`[.fqdomainname]#smtp.FreeBSD.org#:587`", "`smtp.FreeBSD.org:587`")
            documentFile = documentFile.replace("__`__src/__` Subversion Root__", "`_src/_` Subversion Root")
            documentFile = documentFile.replace("`svn+ssh://`[.fqdomainname]#repo.FreeBSD.org#[.filename]#/base#", "`svn+ssh://repo.FreeBSD.org/base`")
            documentFile = documentFile.replace("__`__doc/__` Subversion Root__", "`_doc/_` Subversion Root")
            documentFile = documentFile.replace("`svn+ssh://`[.fqdomainname]#repo.FreeBSD.org#[.filename]#/doc#", "`svn+ssh://repo.FreeBSD.org/doc`")
            documentFile = documentFile.replace("__`__ports/__` Subversion Root__", "`_ports/_` Subversion Root")
            documentFile = documentFile.replace("`svn+ssh://`[.fqdomainname]#repo.FreeBSD.org#[.filename]#/ports#", "`svn+ssh://repo.FreeBSD.org/ports`")
            documentFile = documentFile.replace("__Internal Mailing Lists__", "_Internal Mailing Lists_")
            documentFile = documentFile.replace("[.fqdomainname]#FreeBSD.org#", "`FreeBSD.org`")
            documentFile = documentFile.replace("__Core Team monthly reports__", "_Core Team monthly reports_")
            documentFile = documentFile.replace("__Ports Management Team monthly reports__", "_Ports Management Team monthly reports_")
            documentFile = documentFile.replace("__Noteworthy `__src/__` SVN Branches__", "_Noteworthy `src/` SVN Branches:_")
            documentFile = documentFile.replace("`stable/`_n_ (_n_-STABLE), `head` (-CURRENT)", "`stable/n` (`n`-STABLE), `head` (-CURRENT)")
            documentFile = documentFile.replace("__Login Methods__", "_Login Methods_")
            documentFile = documentFile.replace("__Main Shell Host__", "_Main Shell Host_")
            documentFile = documentFile.replace("__SMTP Host__", "_SMTP Host_")
            documentFile = documentFile.replace(" svn merge -c`r123456`\n^/head/ stable/`11`", "% svn merge -c r123456 ^/head/ stable/11")
            documentFile = documentFile.replace(" svn commit stable/`11`", "% svn commit stable/11")
            documentFile = documentFile.replace(" svn merge -c`r123456`\n^/stable/`11`\nreleng/`11.0`", "% svn merge -c r123456 ^/stable/11  releng/11.0")
            documentFile = documentFile.replace(" svn commit releng/`11.0`", "% svn commit releng/11.0")
            documentFile = documentFile.replace(" svn merge -c`r123456`\n^/head/`checkout`", "% svn merge -c r123456 ^/head/ checkout")
            documentFile = documentFile.replace(" svn commit`checkout`", "% svn commit checkout")
            documentFile = documentFile.replace(" svn merge -c`r123456`\n^/stable/`10`\nreleng/`10.0`", "% svn merge -c r123456 ^/stable/10 releng/10.0")
            documentFile = documentFile.replace("[.acronym]#PG#", "PG")
            documentFile = documentFile.replace("https://reviews.freebsd.org/p/_bob_example.com_/", "https://reviews.freebsd.org/p/bob_example.com/")
            documentFile = documentFile.replace("Your selection?\n% 1", "Your selection? 1")
            documentFile = documentFile.replace("What keysize do you want? (2048)\n% 2048\n<.>", "What keysize do you want? (2048) 2048 <.>")
            documentFile = documentFile.replace("Key is valid for? (0)\n% 3y\n<.>", "Key is valid for? (0) 3y <.>")
            documentFile = documentFile.replace("Is this correct? (y/N)\n% y", "Is this correct? (y/N) y")
            documentFile = documentFile.replace("Real name:\n% Chucky Daemon\n<.>", "Real name: Chucky Daemon <.>")
            documentFile = documentFile.replace("Email address:\n% notreal@example.com", "Email address: notreal@example.com")
            documentFile = documentFile.replace("    \"`Chucky Daemon <notreal@example.com>`\n\"", "\"Chucky Daemon <notreal@example.com>\"")
            documentFile = documentFile.replace("Change (N)ame, (C)omment, (E)mail or (O)kay/(Q)uit?\n% o", "Change (N)ame, (C)omment, (E)mail or (O)kay/(Q)uit? o")
            documentFile = documentFile.replace("+<.>", "<.>")
            documentFile = documentFile.replace("[[conventions-everyone]]", "\n[[conventions-everyone]]")
            documentFile = documentFile.replace("[.command]#svn rm#ed", "[.command]#svn rm#-ed")
            documentFile = documentFile.replace("[.varname]#MLINK#s", "[.varname]#MLINKS#")

        if (name == 'fonts'):
            documentFile = documentFile.replace(" Generated by Fontographer 3.1", "% Generated by Fontographer 3.1")
            documentFile = documentFile.replace("CreationDate: 1/15/91 5:16:03 PM", "%%CreationDate: 1/15/91 5:16:03 PM")
            documentFile = documentFile.replace("VMusage: 1024 45747", "%%VMusage: 1024 45747")
            documentFile = documentFile.replace("!FontType1-1.0: Showboat 001.001", "%!FontType1-1.0: Showboat 001.001")
            documentFile = documentFile.replace("** [.filename]#GS_TTF.PS#", "\n** [.filename]#GS_TTF.PS#")
            documentFile = documentFile.replace("GS>\n% Showboat DoFont", "GS>Showboat DoFont")
            documentFile = documentFile.replace("GS>\n% quit", "GS>quit")

        if (name == 'freebsd-update-server'):
            documentFile = documentFile.replace("Parameters for consideration would be:\n", "Parameters for consideration would be:\n\n")
            documentFile = documentFile.replace("export RELH=1ea1f6f652d7c5f5eab7ef9f8edbed50cb664b08ed761850f95f48e86cc71ef5", "export RELH=1ea1f6f652d7c5f5eab7ef9f8edbed50cb664b08ed761850f95f48e86cc71ef5 <.>")
            documentFile = documentFile.replace("+<.>", "+\n<.>")
            documentFile = documentFile.replace("[.command]#make release#{{<", "[.command]#make release# {{<")
            documentFile = documentFile.replace("` findextradocs ()`", "`findextradocs ()`")
            
        if (name == 'geom-class'):
            documentFile = documentFile.replace("g_consumer*", "g_consumer")

        if (name == 'ldap-auth'):
            documentFile = documentFile.replace("  make install clean", "# make install clean")

        if (name == 'nanobsd'):
            documentFile = documentFile.replace("# cd /usr/src/tools/tools/nanobsd\n<.>\n", "# cd /usr/src/tools/tools/nanobsd <.>")
            documentFile = documentFile.replace("# sh nanobsd.sh\n<.>\n", "# sh nanobsd.sh <.>")
            documentFile = documentFile.replace("# cd /usr/obj/nanobsd.full\n", "# cd /usr/obj/nanobsd.full <.>")
            documentFile = documentFile.replace("# dd if=_.disk.full of=/dev/da0 bs=64k <1>\n<.>", "# dd if=_.disk.full of=/dev/da0 bs=64k <.>")
            documentFile = documentFile.replace("myhost\n# nc -l 2222 < _.disk.image", "myhost# nc -l 2222 < _.disk.image")
            documentFile = documentFile.replace("[.filename]#_.disk.full#", "[.filename]#\_.disk.full#")

            # For german
            documentFile = documentFile.replace("[.application]#NanoBSD#<.> ", "<.> [.application]#NanoBSD#")

        if (name == 'hubs'):
            documentFile = documentFile.replace("* ftp.is.FreeBSD.org - \n+", "* ftp.is.FreeBSD.org - ")
            documentFile = documentFile.replace("* ftp2.ru.FreeBSD.org - \n+", "* ftp2.ru.FreeBSD.org -")
            documentFile = documentFile.replace("(Bandwidth)]http:", "(Bandwidth)] http:")
            documentFile = documentFile.replace("[(FTP processes)]http:", "[(FTP processes)] http:")

        if (name == 'new-users'):
            documentFile = documentFile.replace("Login group is ``jack''. Invite jack into other groups:\n wheel", "Login group is \"jack\". Invite jack into other groups: wheel")
            documentFile = documentFile.replace("`*`", "`\*`")
            documentFile = documentFile.replace("[.command]#/#kbd:[Enter]", "[.command]#/# kbd:[Enter]")
            documentFile = documentFile.replace("[.command]#_n_G#::", "[.command]#nG#::")
            documentFile = documentFile.replace("[.filename]#/cdrom/ports/*/*/pkg/DESCR#", "[.filename]#/cdrom/ports/\*/*/pkg/DESCR#")

            if (language == 'pl'):
                documentFile = documentFile.replace("Login group is ``marek''. Invite marek into other groups:\n wheel", "Login group is \"marek\". Invite marek into other groups: wheel")

        if (name == 'pam'):
            documentFile = documentFile.replace("== Definitions", "=== Definitions")
            documentFile = documentFile.replace("== Usage Examples", "=== Usage Examples")
            documentFile = documentFile.replace("== Client and Server Are One", "==== Client and Server Are One")
            documentFile = documentFile.replace("== Sample Policy", "==== Sample Policy")
            documentFile = documentFile.replace("== Client and Server Are Separate", "==== Client and Server Are Separate")
            documentFile = documentFile.replace("== Facilities and Primitives", "=== Facilities and Primitives")
            documentFile = documentFile.replace("== Modules", "=== Modules")
            documentFile = documentFile.replace("== Module Naming", "==== Module Naming")
            documentFile = documentFile.replace("== Module Versioning", "==== Module Versioning")
            documentFile = documentFile.replace("== Chains and Policies", "=== Chains and Policies")
            documentFile = documentFile.replace("== Transactions", "=== Transactions")
            documentFile = documentFile.replace("== PAM Policy Files", "=== PAM Policy Files")
            documentFile = documentFile.replace("== The [.filename]#/etc/pam.conf#", "==== The [.filename]#/etc/pam.conf#")
            documentFile = documentFile.replace("== The [.filename]#/etc/pam.d#", "==== The [.filename]#/etc/pam.d#")
            documentFile = documentFile.replace("== The Policy Search Order", "==== The Policy Search Order")
            documentFile = documentFile.replace("== Breakdown of a Configuration Line", "=== Breakdown of a Configuration Line")
            documentFile = documentFile.replace("== Policies", "=== Policies")
            documentFile = documentFile.replace("== {{< manpage \"pam\" \"deny\" >}}", "=== {{< manpage \"pam\" \"deny\" >}}")
            documentFile = documentFile.replace("== {{< manpage \"pam\" \"echo\" >}}", "=== {{< manpage \"pam\" \"echo\" >}}")
            documentFile = documentFile.replace("== {{< manpage \"pam\" \"exec\" >}}", "=== {{< manpage \"pam\" \"exec\" >}}")
            documentFile = documentFile.replace("== {{< manpage \"pam\" \"ftpusers\" >}}", "=== {{< manpage \"pam\" \"ftpusers\" >}}")
            documentFile = documentFile.replace("== {{< manpage \"pam\" \"group\" >}}", "=== {{< manpage \"pam\" \"group\" >}}")
            documentFile = documentFile.replace("== {{< manpage \"pam\" \"guest\" >}}", "=== {{< manpage \"pam\" \"guest\" >}}")
            documentFile = documentFile.replace("== {{< manpage \"pam\" \"krb5\" >}}", "=== {{< manpage \"pam\" \"krb5\" >}}")
            documentFile = documentFile.replace("== {{< manpage \"pam\" \"ksu\" >}}", "=== {{< manpage \"pam\" \"ksu\" >}}")
            documentFile = documentFile.replace("== {{< manpage \"pam\" \"lastlog\" >}}", "=== {{< manpage \"pam\" \"lastlog\" >}}")
            documentFile = documentFile.replace("== {{< manpage \"pam\" \"login\" >}}", "=== {{< manpage \"pam\" \"login\" >}}")
            documentFile = documentFile.replace("== {{< manpage \"pam\" \"nologin\" >}}", "=== {{< manpage \"pam\" \"nologin\" >}}")
            documentFile = documentFile.replace("== {{< manpage \"pam\" \"opie\" >}}", "=== {{< manpage \"pam\" \"opie\" >}}")
            documentFile = documentFile.replace("== {{< manpage \"pam\" \"opieaccess\" >}}", "=== {{< manpage \"pam\" \"opieaccess\" >}}")
            documentFile = documentFile.replace("== {{< manpage \"pam\" \"passwdqc\" >}}", "=== {{< manpage \"pam\" \"passwdqc\" >}}")
            documentFile = documentFile.replace("== {{< manpage \"pam\" \"permit\" >}}", "=== {{< manpage \"pam\" \"permit\" >}}")
            documentFile = documentFile.replace("== {{< manpage \"pam\" \"radius\" >}}", "=== {{< manpage \"pam\" \"radius\" >}}")
            documentFile = documentFile.replace("== {{< manpage \"pam\" \"rhosts\" >}}", "=== {{< manpage \"pam\" \"rhosts\" >}}")
            documentFile = documentFile.replace("== {{< manpage \"pam\" \"rootok\" >}}", "=== {{< manpage \"pam\" \"rootok\" >}}")
            documentFile = documentFile.replace("== {{< manpage \"pam\" \"securetty\" >}}", "=== {{< manpage \"pam\" \"securetty\" >}}")
            documentFile = documentFile.replace("== {{< manpage \"pam\" \"self\" >}}", "=== {{< manpage \"pam\" \"self\" >}}")
            documentFile = documentFile.replace("== {{< manpage \"pam\" \"ssh\" >}}", "=== {{< manpage \"pam\" \"ssh\" >}}")
            documentFile = documentFile.replace("== {{< manpage \"pam\" \"tacplus\" >}}", "=== {{< manpage \"pam\" \"tacplus\" >}}")
            documentFile = documentFile.replace("== {{< manpage \"pam\" \"unix\" >}}", "=== {{< manpage \"pam\" \"unix\" >}}")
            documentFile = documentFile.replace("Password:\n% xi3kiune", "Password: xi3kiune")
            documentFile = documentFile.replace("  whoami", "# whoami")
            documentFile = documentFile.replace("Welcome to FreeBSD!", "Welcome to FreeBSD!\n%")
            documentFile = documentFile.replace("`alice`{{< manpage \"su\" \"1\" >}}", "`alice` {{< manpage \"su\" \"1\" >}}")
            documentFile = documentFile.replace("include::{sourcedir}/pam_unix.c[]", "include::static/source/articles/pam/pam_unix.c[]")
            documentFile = documentFile.replace("include::{sourcedir}/converse.c[]", "include::static/source/articles/pam/converse.c[]")
            documentFile = documentFile.replace("include::{sourcedir}/su.c[]", "include::static/source/articles/pam/su.c[]")

        if (name == 'releng'):
            documentFile = documentFile.replace("_\"", "_ \"")
            documentFile = documentFile.replace("[.command]#make#`buildworld`", "[.command]#make buildworld#")
            documentFile = documentFile.replace("https://www.freebsd.org/support/bugreports.html", "https://www.freebsd.org/support/bugreports/")
            documentFile = documentFile.replace("link:#articles.committers-guide;/subversion-primer/#subversion-primer-base-layout[Committer's Guide]", "")
            documentFile = documentFile.replace("footnote:[#books.handbook;/network-pxe-nfs.html]", "footnote:[{{< books \"handbook\" \"Diskless Operation with PXE\" \"network-diskless\">}}]")
            documentFile = documentFile.replace("footnote:[FreeBSD committers]", "footnote:[{{< articles \"contributors\" \"FreeBSD committers\" \"#staff-committers\">}}]")
            documentFile = documentFile.replace("footnote:[FreeBSD Core Team]", "footnote:[link:https://www.FreeBSD.org/administration/#t-core[FreeBSD Core Team]]")
            documentFile = documentFile.replace("footnote:[Rebuilding \"world\"]", "footnote:[{{< books \"handbook\" \"Rebuilding world\" \"makeworld\">}}]")
            documentFile = documentFile.replace("/stage/cdrom\n# find . -type f | sed -e 's/^\.\///' | sort > filename.txt", "/stage/cdrom# find . -type f | sed -e 's/^\.\///' | sort > filename.txt")
            documentFile = documentFile.replace("footnote:[Marshall Kirk McKusick, Michael J. Karels, and Keith Bostic: The Release Engineering of 4.3BSD]", "footnote:[Marshall Kirk McKusick, Michael J. Karels, and Keith Bostic: link:http://docs.FreeBSD.org/44doc/papers/releng.html[The Release Engineering of 4.3BSD]]")

        if (name == 'remote-install'):
            documentFile = documentFile.replace("[[requirements]]", "\n[[requirements]]")
            documentFile = documentFile.replace("[.application]#mfsBSD#http://mfsbsd.vx.sk/[home page]", "[.application]#mfsBSD# http://mfsbsd.vx.sk/[home page]")
            documentFile = documentFile.replace("# fdisk -BI /dev/ad0\n<.>\n", "# fdisk -BI /dev/ad0 <.>")
            documentFile = documentFile.replace("# bsdlabel -wB /dev/ad0s1\n<.>\n", "# bsdlabel -wB /dev/ad0s1 <.>")
            documentFile = documentFile.replace("# bsdlabel -e /dev/ad0s1\n<.>\n", "# bsdlabel -e /dev/ad0s1 <.>")
            documentFile = documentFile.replace("# bsdlabel /dev/ad0s1 > /tmp/bsdlabel.txt && bsdlabel -R /dev/ad1s1 /tmp/bsdlabel.txt\n<.>\n", "# bsdlabel /dev/ad0s1 > /tmp/bsdlabel.txt && bsdlabel -R /dev/ad1s1 /tmp/bsdlabel.txt <.>")
            documentFile = documentFile.replace("# gmirror label root /dev/ad[01]s1a\n", "# gmirror label root /dev/ad[01]s1a <.>")
            documentFile = documentFile.replace("# gmirror label -F swap /dev/ad[01]s1b\n", "# gmirror label -F swap /dev/ad[01]s1b <.>")
            documentFile = documentFile.replace("# newfs /dev/mirror/root <2>\n<.>\n", "# newfs /dev/mirror/root <.>")
            documentFile = documentFile.replace("# gmirror label var /dev/ad[01]s1d <1>", "# gmirror label var /dev/ad[01]s1d")

        if (name == 'rc-scripting'):
            documentFile = documentFile.replace("An interpreted script", "&#10122; An interpreted script")
            documentFile = documentFile.replace("In [.filename]#/etc/rc.subr#", "&#10123; In [.filename]#/etc/rc.subr#")
            documentFile = documentFile.replace("[[name-var]]", "&#10124; [[name-var]]")
            documentFile = documentFile.replace("The main idea behind", "&#10125; The main idea behind")
            documentFile = documentFile.replace("We should keep", "&#10126; We should keep")
            documentFile = documentFile.replace("The body of a", "&#10127; The body of a")
            documentFile = documentFile.replace("This call to", "&#10128; This call to")
            documentFile = documentFile.replace("Usually this", "&#10129; Usually this")
            documentFile = documentFile.replace("The variable [.var]#rcvar#", "&#10122; The variable [.var]#rcvar#")
            documentFile = documentFile.replace("Now `load_rc_config`", "&#10123; Now `load_rc_config`")
            documentFile = documentFile.replace("A warning will be emitted", "&#10124; A warning will be emitted")
            documentFile = documentFile.replace("Now the message to be", "&#10125; Now the message to be")
            documentFile = documentFile.replace("Here we use", "&#10126; Here we use")
            documentFile = documentFile.replace("The [.var]#command#", "&#10122; The [.var]#command#")
            documentFile = documentFile.replace("Additional arguments to", "&#10122; Additional arguments to")
            documentFile = documentFile.replace("A good-mannered daemon", "&#10123; A good-mannered daemon")
            documentFile = documentFile.replace("If the daemon cannot run unless", "&#10124; If the daemon cannot run unless")
            documentFile = documentFile.replace("We can customize signals", "&#10125; We can customize signals")
            documentFile = documentFile.replace("Performing additional", "&#10126;&#10127; Performing additional")
            documentFile = documentFile.replace("If we would like", "&#10128; If we would like")
            documentFile = documentFile.replace("Our script supports", "&#10129;&#9454; Our script supports")
            documentFile = documentFile.replace("A script can invoke", "&#9453; A script can invoke")
            documentFile = documentFile.replace("A handy function named", "&#10130; A handy function named")
            documentFile = documentFile.replace("[[rc-flags]]", "&#10131; [[rc-flags]]")
            documentFile = documentFile.replace("In certain cases we may need to emit an important message", "&#9451; In certain cases we may need to emit an important message")
            documentFile = documentFile.replace("The exit codes", "&#9452; The exit codes")
            documentFile = documentFile.replace("That line declares the names", "&#10122; That line declares the names")
            documentFile = documentFile.replace("So our script indicates", "&#10123;&#10124; So our script indicates")
            documentFile = documentFile.replace("As we remember from", "&#10125; As we remember from")
            documentFile = documentFile.replace("To begin with", "&#10126; To begin with")
            documentFile = documentFile.replace("All arguments you", "&#10122; All arguments you")
            documentFile = documentFile.replace("The same applies to any", "&#10123; The same applies to any")
            documentFile = documentFile.replace("If we want just to", "&#10124; If we want just to")
            documentFile = documentFile.replace("<[.filename]#bsd.prog.mk#>", "[.filename]#bsd.prog.mk#")
            documentFile = documentFile.replace("<[.filename]#bsd.port.mk#>", "[.filename]#bsd.port.mk#")
            documentFile = documentFile.replace("<<lukem>>", "<<lukem, the original article by Luke Mewburn>>")
            documentFile = documentFile.replace("<<manpages>>", "<<manpages, the respective manual pages>>")
            documentFile = documentFile.replace("<<name-var>>", "<<name-var, earlier>>")
            documentFile = documentFile.replace("<<rc-flags>>", "<<rc-flags, as shown later>>")
            documentFile = documentFile.replace("<<forcedep>>", "<<forcedep, below>>")

        if (name == 'problem-reports'):
            documentFile = documentFile.replace("*\n+ \nhttps://github.com/smileytechguy/", "* https://github.com/smileytechguy/")

        if (name == 'solid-state'):
            documentFile = documentFile.replace("[.command]#make#`install`", "[.command]#make# `install`")

        if (name == 'serial-uart'):
            documentFile = documentFile.replace("Oldach _\n", "Oldach_ ")
            documentFile = documentFile.replace(" to the config file.", "to the config file.")
            documentFile = documentFile.replace(" indicates that the master port is sio16.", "indicates that the master port is sio16.")
            documentFile = documentFile.replace(" will show you the boot messages.", "will show you the boot messages.")
            documentFile = documentFile.replace(" for each device", "for each device")
            documentFile = documentFile.replace("[.filename]#sio#2", "[.filename]#sio#`2`")

        if (name == 'vm-design'):
            documentFile = documentFile.replace("[.question]# In", "[.question]#In {{< manpage \"ls\" \"1\" >}}")

        if (name == 'vinum'):
            documentFile = documentFile.replace("__", "_")
            documentFile = documentFile.replace("{{< manpage \"loader\" \"conf\" >}}", "{{< manpage \"loader.conf\" \"5\" >}}")
            documentFile = documentFile.replace("        size   offset    fstype   [fsize bsize bps/cpg]", "#        size   offset    fstype   [fsize bsize bps/cpg]")
            documentFile = documentFile.replace("  a:   245760      281    4.2BSD     2048 16384     0    (Cyl.    0*- 15*)", "  a:   245760      281    4.2BSD     2048 16384     0  # (Cyl.    0*- 15*)")
            documentFile = documentFile.replace("  c: 71771688        0    unused        0     0          (Cyl.    0 - 4467*)", "  c: 71771688        0    unused        0     0        # (Cyl.    0 - 4467*)")
            documentFile = documentFile.replace("  h: 71771672       16     vinum                         (Cyl.    0*- 4467*)", "  h: 71771672       16     vinum                       # (Cyl.    0*- 4467*)")
            documentFile = documentFile.replace("<<vinum-concat>>", "<<vinum-concat, Concatenated Organization>>")
            documentFile = documentFile.replace("<<vinum-striped>>", "<<vinum-striped, Striped Organization>>")
            documentFile = documentFile.replace("<<vinum-comparison>>", "<<vinum-comparison, [.filename]#vinum# Plex Organizations>>")
            documentFile = documentFile.replace("<<vinum-simple-vol>>", "<<vinum-simple-vol, A Simple [.filename]#vinum# Volume>>")
            documentFile = documentFile.replace("<<vinum-mirrored-vol>>", "<<vinum-mirrored-vol, A Mirrored [.filename]#vinum# Volume>>")
            documentFile = documentFile.replace("<<vinum-striped-vol>>", "<<vinum-striped-vol, A Striped [.filename]#vinum# Volume>>")
            documentFile = documentFile.replace("<<vinum-raid10-vol>>", "<<vinum-raid10-vol, A Mirrored, Striped [.filename]#vinum# Volume>>")
            documentFile = documentFile.replace("<<vinum-root-panic>>", "<<vinum-root-panic, Nothing Boots, the Bootstrap Panics>>")
        
        if (name == 'handbook'):
            documentFile = documentFile.replace("[.filename]#*.img#", "[.filename]#\*.img#")
            documentFile = documentFile.replace("[.prompt]#0 #", "[.prompt]#0 >#")
            documentFile = documentFile.replace("`CPU`s", "`CPUs`")

        # Some manpages
        documentFile = documentFile.replace("{{< manpage \"rc\" \"conf\" >}}", "{{< manpage \"rc.conf\" \"5\" >}}")
        documentFile = documentFile.replace("{{< manpage \"groff\" \"font\" >}}", "{{< manpage \"groff_font\" \"5\" >}}")
        documentFile = documentFile.replace("{{< manpage \"groff\" \"char\" >}}", "{{< manpage \"groff_char\" \"7\" >}}")
        documentFile = documentFile.replace("{{< manpage \"rc\" \"subr\" >}}", "{{< manpage \"rc.subr\" \"8\" >}}")
        documentFile = documentFile.replace("{{< manpage \"openpam\" \"ttyconv\" >}}", "{{< manpage \"openpam_ttyconv\" \"3\" >}}")
        documentFile = documentFile.replace("{{< manpage \"pam\" \"unix\" >}}", "{{< manpage \"pam_unix\" \"8\" >}}")
        documentFile = documentFile.replace("{{< manpage \"pam\" \"get\" >}}", "{{< manpage \" pam_get_authtok\" \"3\" >}}")
        documentFile = documentFile.replace("{{< manpage \"pam\" \"tacplus\" >}}", "{{< manpage \"pam_tacplus\" \"8\" >}}")
        documentFile = documentFile.replace("{{< manpage \"pam\" \"ssh\" >}}", "{{< manpage \"pam_ssh\" \"8\" >}}")
        documentFile = documentFile.replace("{{< manpage \"pam\" \"self\" >}}", "{{< manpage \"pam_self\" \"8\" >}}")
        documentFile = documentFile.replace("{{< manpage \"pam\" \"securetty\" >}}", "{{< manpage \"pam_securetty\" \"8\" >}}")
        documentFile = documentFile.replace("{{< manpage \"pam\" \"rootok\" >}}", "{{< manpage \"pam_rootok\" \"8\" >}}")
        documentFile = documentFile.replace("{{< manpage \"pam\" \"rhosts\" >}}", "{{< manpage \"pam_rhosts\" \"8\" >}}")
        documentFile = documentFile.replace("{{< manpage \"pam\" \"radius\" >}}", "{{< manpage \"pam_radius\" \"8\" >}}")
        documentFile = documentFile.replace("{{< manpage \"pam\" \"permit\" >}}", "{{< manpage \"pam_permit\" \"8\" >}}")
        documentFile = documentFile.replace("{{< manpage \"pam\" \"passwdqc\" >}}", "{{< manpage \"pam_passwdqc\" \"8\" >}}")
        documentFile = documentFile.replace("{{< manpage \"pam\" \"opieaccess\" >}}", "{{< manpage \"pam_opieaccess\" \"8\" >}}")
        documentFile = documentFile.replace("{{< manpage \"pam\" \"opie\" >}}", "{{< manpage \"pam_opie\" \"8\" >}}")
        documentFile = documentFile.replace("{{< manpage \"pam\" \"nologin\" >}}", "{{< manpage \"pam_nologin\" \"8\" >}}")
        documentFile = documentFile.replace("{{< manpage \"pam\" \"login\" >}}", "{{< manpage \"pam_login_access\" \"8\" >}}")
        documentFile = documentFile.replace("{{< manpage \"pam\" \"lastlog\" >}}", "{{< manpage \"pam_lastlog\" \"8\" >}}")
        documentFile = documentFile.replace("{{< manpage \"pam\" \"ksu\" >}}", "{{< manpage \"pam_ksu\" \"8\" >}}")
        documentFile = documentFile.replace("{{< manpage \"pam\" \"krb5\" >}}", "{{< manpage \"pam_krb5\" \"8\" >}}")
        documentFile = documentFile.replace("{{< manpage \"pam\" \"guest\" >}}", "{{< manpage \"pam_guest\" \"8\" >}}")
        documentFile = documentFile.replace("{{< manpage \"pam\" \"group\" >}}", "{{< manpage \"pam_group\" \"8\" >}}")
        documentFile = documentFile.replace("{{< manpage \"pam\" \"ftpusers\" >}}", "{{< manpage \"pam_ftpusers\" \"8\" >}}")
        documentFile = documentFile.replace("{{< manpage \"pam\" \"exec\" >}}", "{{< manpage \"pam_exec\" \"8\" >}}")
        documentFile = documentFile.replace("{{< manpage \"pam\" \"echo\" >}}", "{{< manpage \"pam_echo\" \"8\" >}}")
        documentFile = documentFile.replace("{{< manpage \"pam\" \"deny\" >}}", "{{< manpage \"pam_deny\" \"8\" >}}")
        documentFile = documentFile.replace("{{< manpage \"pam\" \"end\" >}}", "{{< manpage \"pam_end\" \"3\" >}}")
        documentFile = documentFile.replace("{{< manpage \"pam\" \"close\" >}}", "{{< manpage \"pam_close_session\" \"3\" >}}")
        documentFile = documentFile.replace("{{< manpage \"pam\" \"open\" >}}", "{{< manpage \"pam_open_session\" \"3\" >}}")
        documentFile = documentFile.replace("{{< manpage \"pam\" \"authenticate\" >}}", "{{< manpage \"pam_authenticate\" \"3\" >}}")
        documentFile = documentFile.replace("{{< manpage \"pam\" \"set\" >}}", "{{< manpage \"pam_set_item\" \"3\" >}}")
        documentFile = documentFile.replace("{{< manpage \"pam\" \"start\" >}}", "{{< manpage \"pam_start\" \"3\" >}}")
        documentFile = documentFile.replace("{{< manpage \"pam\" \"chauthtok\" >}}", "{{< manpage \"pam_chauthtok\" \"3\" >}}")
        documentFile = documentFile.replace("{{< manpage \"pam\" \"acct\" >}}", "{{< manpage \"pam_acct_mgmt\" \"3\" >}}")
        documentFile = documentFile.replace("{{< manpage \"pam\" \"setcred\" >}}", "{{< manpage \"pam_setcred\" \"3\" >}}")
        documentFile = documentFile.replace("{{< manpage \"login\" \"access\" >}}", "{{< manpage \"login.access\" \"5\" >}}")

        # Replace mailing list..
        documentFile = documentFile.replace("{a.jobs}", "{{< mailing-lists \"freebsd-jobs\" >}}")
        documentFile = documentFile.replace("{a.doc}", "{{< mailing-lists \"freebsd-doc\" >}}")
        documentFile = documentFile.replace("{a.questions.url}link:[information page of the FreeBSD-questions mailing list]", "{{< mailing-lists \"freebsd-questions\" >}}")
        documentFile = documentFile.replace("{a.hubs}", "{{< mailing-lists \"freebsd-hubs\" >}}")
        documentFile = documentFile.replace("{a.current.name}", "{{< mailing-lists \"freebsd-current\" >}}")
        documentFile = documentFile.replace("{a.questions.name}", "{{< mailing-lists \"freebsd-questions\" >}}")
        documentFile = documentFile.replace("{a.mirror-announce}", "{{< mailing-lists \"mirror-announce\" >}}")
        documentFile = documentFile.replace("{a.questions}", "{{< mailing-lists \"freebsd-questions\" >}}")
        documentFile = documentFile.replace("{a.bugs}", "{{< mailing-lists \"freebsd-bugs\" >}}")
        documentFile = documentFile.replace("{a.hackers}", "{{< mailing-lists \"freebsd-hackers\" >}}")
        documentFile = documentFile.replace("{a.committers}", "{{< mailing-lists \"cvs-committers\" \"name\" >}}")
        documentFile = documentFile.replace("{a.developers}", "{{< mailing-lists \"freebsd-developers\" \"name\" >}}")
        documentFile = documentFile.replace("{a.announce}", "{{< mailing-lists \"freebsd-announce\" >}}")
        documentFile = documentFile.replace("{a.current}", "{{< mailing-lists \"freebsd-current\" >}}")
        documentFile = documentFile.replace("{a.core}", "{{< mailing-lists \"freebsd-core\" \"desc-email\" >}}")
        documentFile = documentFile.replace("{a.ports}", "{{< mailing-lists \"freebsd-ports\" >}}")
        documentFile = documentFile.replace("{a.ports-bugs}", "{{< mailing-lists \"freebsd-ports-bugs\" >}}")
        documentFile = documentFile.replace("{a.svn-ports-head}", "{{< mailing-lists \"svn-ports-head\" >}}")
        documentFile = documentFile.replace("{a.svn-src-all.name}", "{{< mailing-lists \"svn-src-all\" \"email\" >}}")
        documentFile = documentFile.replace("{a.svn-ports-all.name}", "{{< mailing-lists \"svn-ports-all\" \"email\" >}}")
        documentFile = documentFile.replace("{a.svn-doc-all.name}", "{{< mailing-lists \"svn-doc-all\" \"email\" >}}")
        documentFile = documentFile.replace("{a.arch}", "{{< mailing-lists \"freebsd-arch\" >}}")
        documentFile = documentFile.replace("{a.bugfollowup}", "{{< mailing-lists \"bugfollowup\" \"desc-email\" >}}")
        documentFile = documentFile.replace("{a.majordomo}", "{{< mailing-lists \"majordomo\" \"desc-email\" >}}")
        documentFile = documentFile.replace("{a.qa}", "{{< mailing-lists \"freebsd-qa\" >}}")
        documentFile = documentFile.replace("{a.bugs.name}", "{{< mailing-lists \"freebsd-bugs\" >}}")
        documentFile = documentFile.replace("{a.doc-developers}", "{{< mailing-lists \"doc-developers\" \"desc\" >}}")
        documentFile = documentFile.replace("{a.doc-committers}", "{{< mailing-lists \"doc-committers\" \"desc\" >}}")
        documentFile = documentFile.replace("{a.ports-developers}", "{{< mailing-lists \"ports-developers\" \"desc\" >}}")
        documentFile = documentFile.replace("{a.ports-committers}", "{{< mailing-lists \"ports-committers\" \"desc\" >}}")
        documentFile = documentFile.replace("{a.src-developers}", "{{< mailing-lists \"freebsd-src-developers\" \"desc\" >}}")
        documentFile = documentFile.replace("{a.src-committers}", "{{< mailing-lists \"freebsd-src-committers\" \"desc\" >}}")
        documentFile = documentFile.replace("{a.cvsall}", "{{< mailing-lists \"cvs-all\" >}}")

        # Replacing emails
        documentFile = documentFile.replace("{a.cperciva.email}", "{{< get-authors \"cperciva\" \"both\" >}}")
        documentFile = documentFile.replace("{a.pjd.email}", "{{< get-authors \"pjd\" \"both\" >}}")
        documentFile = documentFile.replace("{a.ivoras.email}", "{{< get-authors \"ivoras\" \"both\" >}}")
        documentFile = documentFile.replace("{a.pjd}", "{{< get-authors \"pjd\" \"name\" >}}")
        documentFile = documentFile.replace("{a.ivoras}", "{{< get-authors \"ivoras\" \"name\" >}}")
        documentFile = documentFile.replace("{a.jhb.email}", "{{< get-authors \"jhb\" \"both\" >}}")
        documentFile = documentFile.replace("{a.kib.email}", "{{< get-authors \"kib\" \"both\" >}}")
        documentFile = documentFile.replace("{a.jkim.email}", "{{< get-authors \"jkim\" \"both\" >}}")
        documentFile = documentFile.replace("{a.netchild.email}", "{{< get-authors \"netchild\" \"both\" >}}")
        documentFile = documentFile.replace("{a.ssouhlal.email}", "{{< get-authors \"ssouhlal\" \"both\" >}}")
        documentFile = documentFile.replace("{a.davidxu.email}", "{{< get-authors \"davidxu\" \"both\" >}}")
        documentFile = documentFile.replace("{a.grog.email}", "{{< get-authors \"grog\" \"both\" >}}")
        documentFile = documentFile.replace("{a.linimon.email}", "{{< get-authors \"linimon\" \"both\" >}}")
        documentFile = documentFile.replace("{a.asami.email}", "{{< get-authors \"asami\" \"both\" >}}")
        documentFile = documentFile.replace("{a.steve.email}", "{{< get-authors \"steve\" \"both\" >}}")
        documentFile = documentFile.replace("{a.bmah.email}", "{{< get-authors \"bmah\" \"both\" >}}")
        documentFile = documentFile.replace("{a.nik.email}", "{{< get-authors \"nik\" \"both\" >}}")
        documentFile = documentFile.replace("{a.obrien.email}", "{{< get-authors \"obrien\" \"both\" >}}")
        documentFile = documentFile.replace("{a.kris.email}", "{{< get-authors \"kris\" \"both\" >}}")
        documentFile = documentFile.replace("{a.jhb.email}", "{{< get-authors \"jhb\" \"both\" >}}")
        documentFile = documentFile.replace("{a.rgrimes.email}", "{{< get-authors \"rgrimes\" \"both\" >}}")
        documentFile = documentFile.replace("{a.phk.email}", "{{< get-authors \"phk\" \"both\" >}}")
        documentFile = documentFile.replace("{a.re}", "{{< get-authors \"re\" \"email\" >}}")
        documentFile = documentFile.replace("{a.uhclem.email}", "{{< get-authors \"uhclem\" \"both\" >}}")
        documentFile = documentFile.replace("{a.awebster.email}", "{{< get-authors \"awebster\" \"both\" >}}")
        documentFile = documentFile.replace("{a.whiteside.email}", "{{< get-authors \"whiteside\" \"both\" >}}")
        documentFile = documentFile.replace("{a.nsayer.email}", "{{< get-authors \"nsayer\" \"both\" >}}")
        documentFile = documentFile.replace("{a.des.email}", "{{< get-authors \"des\" \"both\" >}}")
        documentFile = documentFile.replace("{a.bde.email}", "{{< get-authors \"bde\" \"both\" >}}")
        documentFile = documentFile.replace("{a.mharo.email}", "{{< get-authors \"mharo\" \"both\" >}}")
        documentFile = documentFile.replace("{a.will.email}", "{{< get-authors \"will\" \"both\" >}}")
        documentFile = documentFile.replace("{a.garga.email}", "{{< get-authors \"garga\" \"both\" >}}")
        documentFile = documentFile.replace("{a.crees.email}", "{{< get-authors \"crees\" \"both\" >}}")
        documentFile = documentFile.replace("{a.phk}", "{{< get-authors \"phk\" \"name\" >}}")
        documentFile = documentFile.replace("{{< manpage \"wollman\" \"email\" >}}", "{{< get-authors \"wollman\" \"both\" >}}")
        documentFile = documentFile.replace("{a.re.members.email}", "{{< get-authors \"re\" \"members\" >}}")
        documentFile = documentFile.replace("{a.so.email}", "{{< get-authors \"so\" \"both\" >}}")
        documentFile = documentFile.replace("{a.so}", "{{< get-authors \"so\" \"name\" >}}")
        documentFile = documentFile.replace("{a.irc.email}", "{{< get-authors \"irc\" \"both\" >}}")
        documentFile = documentFile.replace("{a.arch}", "{{< get-authors \"irc\" \"email\" >}}")
        documentFile = documentFile.replace("{a.asami}", "{{< get-authors \"asami\" \"name\" >}}")
        documentFile = documentFile.replace("{a.steve}", "{{< get-authors \"steve\" \"name\" >}}")
        documentFile = documentFile.replace("{a.bmah}", "{{< get-authors \"bmah\" \"name\" >}}")
        documentFile = documentFile.replace("{a.nik}", "{{< get-authors \"nik\" \"name\" >}}")
        documentFile = documentFile.replace("{a.obrien}", "{{< get-authors \"obrien\" \"name\" >}}")
        documentFile = documentFile.replace("{a.kris}", "{{< get-authors \"kris\" \"name\" >}}")
        documentFile = documentFile.replace("{a.jhb}", "{{< get-authors \"jhb\" \"name\" >}}")
        documentFile = documentFile.replace("{a.rgrimes}", "{{< get-authors \"rgrimes\" \"name\" >}}")
        documentFile = documentFile.replace("{a.keramida}", "{{< get-authors \"keramida\" \"name\" >}}")
        documentFile = documentFile.replace("{a.mm}", "{{< get-authors \"mm\" \"name\" >}}")
        documentFile = documentFile.replace("{a.peter.email}", "{{< get-authors \"peter\" \"both\" >}}")
        documentFile = documentFile.replace("{a.markm.email}", "{{< get-authors \"markm\" \"both\" >}}")
        documentFile = documentFile.replace("{a.joe.email}", "{{< get-authors \"joe\" \"both\" >}}")
        documentFile = documentFile.replace("{a.marcus.email}", "{{< get-authors \"marcus\" \"both\" >}}")
        documentFile = documentFile.replace("{a.kuriyama.email}", "{{< get-authors \"kuriyama\" \"both\" >}}")
        documentFile = documentFile.replace("{a.fenner.email}", "{{< get-authors \"fenner\" \"both\" >}}")
        documentFile = documentFile.replace("{a.ru.email}", "{{< get-authors \"ru\" \"both\" >}}")
        documentFile = documentFile.replace("{a.murray.email}", "{{< get-authors \"murray\" \"both\" >}}")
        documentFile = documentFile.replace("{a.jdp}", "{{< get-authors \"jdp\" \"name\" >}}")
        documentFile = documentFile.replace("{a.peter}", "{{< get-authors \"peter\" \"name\" >}}")
        documentFile = documentFile.replace("{a.markm}", "{{< get-authors \"markm\" \"name\" >}}")
        documentFile = documentFile.replace("{a.bde}", "{{< get-authors \"bde\" \"name\" >}}")
        documentFile = documentFile.replace("{a.dg}", "{{< get-authors \"dg\" \"name\" >}}")
        documentFile = documentFile.replace("{a.jkh}", "{{< get-authors \"jkh\" \"name\" >}}")
        documentFile = documentFile.replace("{a.brian}", "{{< get-authors \"brian\" \"name\" >}}")
        documentFile = documentFile.replace("{a.joerg}", "{{< get-authors \"joerg\" \"name\" >}}")
        documentFile = documentFile.replace("{a.attilio.email}", "{{< get-authors \"attilio\" \"both\" >}}")


        documentFile = documentFile.replace("{a.mbarkah.email}", "{{< get-authors \"mbarkah\" \"both\" >}}")
        documentFile = documentFile.replace("{a.dillon.email}", "{{< get-authors \"dillon\" \"both\" >}}")
        documentFile = documentFile.replace("{a.sef.email}", "{{< get-authors \"sef\" \"both\" >}}")
        documentFile = documentFile.replace("{a.chuckr.email}", "{{< get-authors \"chuckr\" \"both\" >}}")
        documentFile = documentFile.replace("{a.wilko.email}", "{{< get-authors \"wilko\" \"both\" >}}")
        documentFile = documentFile.replace("{a.kuku.email}", "{{< get-authors \"kuku\" \"both\" >}}")
        documentFile = documentFile.replace("{a.joerg.email}", "{{< get-authors \"joerg\" \"both\" >}}")

        # Replace teams
        documentFile = documentFile.replace("{a.admins}", "{{< get-teams \"admins\" >}}")
        documentFile = documentFile.replace("{a.doceng}", "{{< get-teams \"doceng\" >}}")
        documentFile = documentFile.replace("{a.portmgr}", "{{< get-teams \"portmgr\" >}}")
        documentFile = documentFile.replace("{a.security-officer}", "{{< get-teams \"security-officer\" >}}")
        documentFile = documentFile.replace("{a.cvsadm}", "{{< get-teams \"cvsadm\" >}}")

        # Replace releases entities
        documentFile = documentFile.replace("{rel.current}", "{{< param \"releases.relCurrent\" >}}")
        documentFile = documentFile.replace("{rel1.current}", "{{< param \"releases.rel1Current\" >}}")
        documentFile = documentFile.replace("releases.rel121Current", "{{< param \"releases.rel121Current\" >}}")

    # Write all the changes
    fon = open(inputfile, "wt")
    fon.write(documentFile)
    fon.close()

    # Remove emtpy spaces after the + character
    documentFile = '' # clean the file
    if (name != 'fonts'           and 
        name != 'leap-seconds'    and
        name != 'hubs'            and
        name != 'linux-emulation' and
        name != 'new-users'       and
        name != 'releng'          and
        name != 'solid-state'     and
        name != 'vinum'):
        with open(inputfile, 'rt') as fp:
            line = fp.readline()

            lastPlusCharacter = False
            while line:
                if len(line) == 2 and line.strip() == '+':
                    lastPlusCharacter = True

                if lastPlusCharacter == True and len(line.strip()) == 0:
                    lastPlusCharacter = False
                else:
                    documentFile += line
                line = fp.readline()

        # Write all the changes
        fon = open(inputfile, "wt")
        fon.write(documentFile)
        fon.close()

    print("Process finished")
    
if __name__ == "__main__":
    main(sys.argv[1:])
