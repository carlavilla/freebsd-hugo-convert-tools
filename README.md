# FreeBSD Hugo Conversion Tools

The idea of this project is to convert news, events, commercial advisories, etc... from XML to JSON for use on the new Hugo-based website.

## Getting Started

This project will launch ONE time, the code is a mess, but it is not a program that is going to be launched daily or anything like that.

### Requirements

Python 3.7, py37-dateutil

### Use

* advisories-convert.py

```sh
python3.7 advisories-convert.py -i advisories.xml -o advisories.toml
```

* commercial-convert.py

```sh
commercial-convert.py -l consulting -i commercial.consult.xml -o consulting.toml
```

* errata-convert.py

```sh
python3.7 errata-convert.py -i notices.xml -o notices.toml
```

* events-convert.py

```sh
python3.7 events-convert.py -y 2020 -i events2020.xml -o events2020.toml
```

* news-convert.py

```sh
python3.7 news-convert.py -i news.xml -o news.toml
```

* press-convert.py

```sh
python3.7 press-convert.py -i press.xml -o press.toml
```

* In the conversion of the autors, add the end add manually

'''toml
# Below we list the various hats.

# FreeBSD Bugmeister
[bugmeister]
members = [ "eadler@FreeBSD.org", "gavin@FreeBSD.org", "mva@FreeBSD.org", "koobs@FreeBSD.org", "gonzo@FreeBSD.org"]

# FreeBSD Core Team
[core]
members = [ "allanjude@FreeBSD.org", "bcr@FreeBSD.org", "brooks@FreeBSD.org", "imp@FreeBSD.org", "hrs@FreeBSD.org", "jeff@FreeBSD.org", "jhb@FreeBSD.org", "kmoore@FreeBSD.org", "seanc@FreeBSD.org" ]

# FreeBSD Doc Engineering Team
[doceng]
members = [ "gjb@FreeBSD.org", "wblock@FreeBSD.org", "blackend@FreeBSD.org", "gabor@FreeBSD.org", "bcr@FreeBSD.org", "hrs@FreeBSD.org", "ryusuke@FreeBSD.org"]
secretary = "doceng-secretary@FreeBSD.org"

# FreeBSD Donations Liaison
[donations]
members = [ "gahr@FreeBSD.org", "pgollucci@FreeBSD.org", "skreuzer@FreeBSD.org", "koobs@FreeBSD.org", "obrien@FreeBSD.org", "trhodes@FreeBSD.org", "ds@FreeBSD.org", "rwatson@FreeBSD.org" ]

# FreeBSD Freenode IRC Liasons
[irc]
email = "irc@FreeBSD.org"

# FreeBSD Ports Manager Team
[portmgr]
members = [ "mat@FreeBSD.org", "antoine@FreeBSD.org", "bapt@FreeBSD.org", "bdrewery@FreeBSD.org", "rene@FreeBSD.org", "swills@FreeBSD.org", "adamw@FreeBSD.org" ]

# FreeBSD Release Engineering Teams
[re]
email = "re@FreeBSD.org"
members = [ "gjb@FreeBSD.org", "kib@FreeBSD.org", "bdrewery@FreeBSD.org", "blackend@FreeBSD.org", "delphij@FreeBSD.org", "hrs@FreeBSD.org", "glebius@FreeBSD.org", "marius@FreeBSD.org" ]
builders = [ "marcel@FreeBSD.org", "nyan@FreeBSD.org", "nwhitehorn@FreeBSD.org" ]

# FreeBSD Security Officer
[so]
officer = "Gordon Tetlow"
email_officer = "gordon@FreeBSD.org"
members = [ "brooks@FreeBSD.org", "bz@FreeBSD.org", "delphij@FreeBSD.org", "des@FreeBSD.org", "emaste@FreeBSD.org", "gjb@FreeBSD.org", "gnn@FreeBSD.org", "miwi@FreeBSD.org", "@FreeBSD.orgphilip" ]

# FreeBSD cluster entities
[keymaster]
name = "Self-Serve SSH key changer"
email = "keymaster@FreeBSD.org"
'''




[.informaltable]
[cols="10%,10%,80%", frame="none", options="header"]
|===
| I/O Port
| Access Allowed
| Description

|+0x00
|write (DLAB==0)
|

Transmit Holding Register (THR).

Information written to this port are treated as data words and will be transmitted by the UART.

|+0x00
|read (DLAB==0)
|

Receive Buffer Register (RBR).

Any data words received by the UART form the serial link are accessed by the host by reading this port.

|+0x00
|write/read (DLAB==1)
|

Divisor Latch LSB (DLL)

This value will be divided from the master input clock (in the IBM PC, the master clock is 1.8432MHz) and the resulting clock will determine the baud rate of the UART. This register holds bits 0 thru 7 of the divisor.

|+0x01
|write/read (DLAB==1)
|

Divisor Latch MSB (DLH)

This value will be divided from the master input clock (in the IBM PC, the master clock is 1.8432MHz) and the resulting clock will determine the baud rate of the UART. This register holds bits 8 thru 15 of the divisor.

|+0x01
|write/read (DLAB==0)
|Interrupt Enable Register (IER) +

The 8250/16450/16550 UART classifies events into one of four categories. Each category can be configured to generate an interrupt when any of the events occurs. The 8250/16450/16550 UART generates a single external interrupt signal regardless of how many events in the enabled categories have occurred. It is up to the host processor to respond to the interrupt and then poll the enabled interrupt categories (usually all categories have interrupts enabled) to determine the true cause(s) of the interrupt. +
Bit 7 -> Reserved, always 0. +
Bit 6 -> Reserved, always 0. +
Bit 5 -> Reserved, always 0. +
Bit 4 -> Reserved, always 0. +
Bit 3 -> Enable Modem Status Interrupt (EDSSI). Setting this bit to "1" allows the UART to generate an interrupt when a change occurs on one or more of the status lines. +
Bit 2 -> Enable Receiver Line Status Interrupt (ELSI) Setting this bit to "1" causes the UART to generate an interrupt when the an error (or a BREAK signal) has been detected in the incoming data. +
Bit 1 -> Enable Transmitter Holding Register Empty Interrupt (ETBEI) Setting this bit to "1" causes the UART to generate an interrupt when the UART has room for one or more additional characters that are to be transmitted. +
Bit 0 -> Enable Received Data Available Interrupt (ERBFI) Setting this bit to "1" causes the UART to generate an interrupt when the UART has received enough characters to exceed the trigger level of the FIFO, or the FIFO timer has expired (stale data), or a single character has been received when the FIFO is disabled.

|+0x02
|write
|FIFO Control Register (FCR) (This port does not exist on the 8250 and 16450 UART.) +
Bit 7 -> Receiver Trigger Bit #1 +
Bit 6 -> Receiver Trigger Bit #0 +

These two bits control at what point the receiver is to generate an interrupt when the FIFO is active. +
7 6 How many words are received before an interrupt is generated +
0 0 1 +
0 1 4 +
1 0 8 +
1 1 14 +
Bit 5 -> Reserved, always 0. +
Bit 4 -> Reserved, always 0. +
Bit 3 -> DMA Mode Select. If Bit 0 is set to "1" (FIFOs enabled), setting this bit changes the operation of the -RXRDY and -TXRDY signals from Mode 0 to Mode 1. +
Bit 2 -> Transmit FIFO Reset. When a "1" is written to this bit, the contents of the FIFO are discarded. Any word currently being transmitted will be sent intact. This function is useful in aborting transfers. +
Bit 1 -> Receiver FIFO Reset. When a "1" is written to this bit, the contents of the FIFO are discarded. Any word currently being assembled in the shift register will be received intact. +
Bit 0 -> 16550 FIFO Enable. When set, both the transmit and receive FIFOs are enabled. Any contents in the holding register, shift registers or FIFOs are lost when FIFOs are enabled or disabled. +

|+0x02
|read
|Interrupt Identification Register +
Bit 7 -> FIFOs enabled. On the 8250/16450 UART, this bit is zero. +
Bit 6 -> FIFOs enabled. On the 8250/16450 UART, this bit is zero. +
Bit 5 -> Reserved, always 0. +
Bit 4 -> Reserved, always 0. +
Bit 3 -> Interrupt ID Bit #2. On the 8250/16450 UART, this bit is zero. +
Bit 2 -> Interrupt ID Bit #1 +
Bit 1 -> Interrupt ID Bit #0.These three bits combine to report the category of event that caused the interrupt that is in progress. These categories have priorities, so if multiple categories of events occur at the same time, the UART will report the more important events first and the host must resolve the events in the order they are reported. All events that caused the current interrupt must be resolved before any new interrupts will be generated. (This is a limitation of the PC architecture.) +
2 1 0 Priority Description +
0 1 1 First Received Error (OE, PE, BI, or FE) +
0 1 0 Second Received Data Available +
1 1 0 Second Trigger level identification (Stale data in receive buffer) +
0 0 1 Third Transmitter has room for more words (THRE) +
0 0 0 Fourth Modem Status Change (-CTS, -DSR, -RI, or -DCD) +
Bit 0 -> Interrupt Pending Bit. If this bit is set to "0", then at least one interrupt is pending.

|+0x03
|write/read
|Line Control Register (LCR) +
Bit 7 -> Divisor Latch Access Bit (DLAB). When set, access to the data transmit/receive register (THR/RBR) and the Interrupt Enable Register (IER) is disabled. Any access to these ports is now redirected to the Divisor Latch Registers. Setting this bit, loading the Divisor Registers, and clearing DLAB should be done with interrupts disabled. +
Bit 6 -> Set Break. When set to "1", the transmitter begins to transmit continuous Spacing until this bit is set to "0". This overrides any bits of characters that are being transmitted. +
Bit 5 -> Stick Parity. When parity is enabled, setting this bit causes parity to always be "1" or "0", based on the value of Bit 4.
Bit 4 -> Even Parity Select (EPS). When parity is enabled and Bit 5 is "0", setting this bit causes even parity to be transmitted and expected. Otherwise, odd parity is used. +
Bit 3 -> Parity Enable (PEN). When set to "1", a parity bit is inserted between the last bit of the data and the Stop Bit. The UART will also expect parity to be present in the received data. +
Bit 2 -> Number of Stop Bits (STB). If set to "1" and using 5-bit data words, 1.5 Stop Bits are transmitted and expected in each data word. For 6, 7 and 8-bit data words, 2 Stop Bits are transmitted and expected. When this bit is set to "0", one Stop Bit is used on each data word. +
Bit 1 -> Word Length Select Bit #1 (WLSB1) +
Bit 0 -> Word Length Select Bit #0 (WLSB0) +
Together these bits specify the number of bits in each data word. +
1 0 Word Length +
0 0 5 Data Bits +
0 1 6 Data Bits +
1 0 7 Data Bits +
1 1 8 Data Bits +

|+0x04
|write/read
|Modem Control Register (MCR) +
Bit 7 -> Reserved, always 0. +
Bit 6 -> Reserved, always 0. +
Bit 5 -> Reserved, always 0. +
Bit 4 -> Loop-Back Enable. When set to "1", the UART transmitter and receiver are internally connected together to allow diagnostic operations. In addition, the UART modem control outputs are connected to the UART modem control inputs. CTS is connected to RTS, DTR is connected to DSR, OUT1 is connected to RI, and OUT 2 is connected to DCD. +
Bit 3 OUT 2. -> An auxiliary output that the host processor may set high or low. In the IBM PC serial adapter (and most clones), OUT 2 is used to tri-state (disable) the interrupt signal from the 8250/16450/16550 UART. +
Bit 2 OUT 1. -> An auxiliary output that the host processor may set high or low. This output is not used on the IBM PC serial adapter. +
Bit 1 -> Request to Send (RTS). When set to "1", the output of the UART -RTS line is Low (Active). +
Bit 0 -> Data Terminal Ready (DTR). When set to "1", the output of the UART -DTR line is Low (Active). +

|+0x05
|write/read
|Line Status Register (LSR) +
Bit 7 -> Error in Receiver FIFO. On the 8250/16450 UART, this bit is zero. This bit is set to "1" when any of the bytes in the FIFO have one or more of the following error conditions: PE, FE, or BI. +
Bit 6 -> Transmitter Empty (TEMT). When set to "1", there are no words remaining in the transmit FIFO or the transmit shift register. The transmitter is completely idle. +
Bit 5 -> Transmitter Holding Register Empty (THRE). When set to "1", the FIFO (or holding register) now has room for at least one additional word to transmit. The transmitter may still be transmitting when this bit is set to "1". +
Bit 4 -> Break Interrupt (BI). The receiver has detected a Break signal. +
Bit 3 -> Framing Error (FE). A Start Bit was detected but the Stop Bit did not appear at the expected time. The received word is probably garbled. +
Bit 2 -> Parity Error (PE). The parity bit was incorrect for the word received. +
Bit 1 -> Overrun Error (OE). A new word was received and there was no room in the receive buffer. The newly-arrived word in the shift register is discarded. On 8250/16450 UARTs, the word in the holding register is discarded and the newly- arrived word is put in the holding register. +
Bit 0 -> Data Ready (DR) One or more words are in the receive FIFO that the host may read. A word must be completely received and moved from the shift register into the FIFO (or holding register for 8250/16450 designs) before this bit is set.

|+0x06
|write/read
|Modem Status Register (MSR) +
Bit 7 -> Data Carrier Detect (DCD). Reflects the state of the DCD line on the UART. +
Bit 6 -> Ring Indicator (RI). Reflects the state of the RI line on the UART. +
Bit 5 -> Data Set Ready (DSR). Reflects the state of the DSR line on the UART. +
Bit 4 -> Clear To Send (CTS). Reflects the state of the CTS line on the UART. +
Bit 3 -> Delta Data Carrier Detect (DDCD). Set to "1" if the -DCD line has changed state one more time since the last time the MSR was read by the host. +
Bit 2 -> Trailing Edge Ring Indicator (TERI). Set to "1" if the -RI line has had a low to high transition since the last time the MSR was read by the host. +
Bit 1 -> Delta Data Set Ready (DDSR). Set to "1" if the -DSR line has changed state one more time since the last time the MSR was read by the host. +
Bit 0 -> Delta Clear To Send (DCTS). Set to "1" if the -CTS line has changed state one more time since the last time the MSR was read by the host. +
|+0x07
|write/read
|Scratch Register (SCR). This register performs no function in the UART. Any value can be written by the host to this location and read by the host later on.
|===

## Authors

* **Sergio Carlavilla**(https://www.carlavilla.es)
